﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class gemcontr : MonoBehaviour {

    public Transform redObj;
    public Transform blueObj;
    public Transform greenObj;
    public Transform yellowObj;

    public static string lockDestroy = "n";
    public int rowNum;
    public int colNum;
    public int whichGem;
    public static float destroyGemX;

	// Use this for initialization
	void Start ()

    {
        for (rowNum = -2; rowNum < 6; rowNum++)
        {
            for (colNum = -2; colNum < 4; colNum++)
            {


                Crate();

            }
        }
    }
	
	// Update is called once per frame
	void Update ()
    {


        if (Input.GetMouseButtonDown(0))
        {
            
            if (lockDestroy == "n")
            {
                Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    destroyGemX = hit.collider.gameObject.transform.position.x;
                    Destroy(hit.collider.gameObject);

                    Fill();
                    lockDestroy = "y";
                    
                }StartCoroutine(Wait(1));
            }
            
        }
        


    }
    public void Crate()
    {


        whichGem = UnityEngine.Random.Range(1, 5);
        if (whichGem == 1)
        {
            Instantiate(redObj, new Vector2(colNum, rowNum), redObj.rotation);
        }
        if (whichGem == 2)
        {
            Instantiate(blueObj, new Vector2(colNum, rowNum), blueObj.rotation);
        }
        if (whichGem == 3)
        {
            Instantiate(greenObj, new Vector2(colNum, rowNum), greenObj.rotation);
        }
        if (whichGem > 3)
        {
            Instantiate(yellowObj, new Vector2(colNum, rowNum), yellowObj.rotation);
        }


    }

    public void Fill()
    {


        whichGem = UnityEngine.Random.Range(1, 5);
        if (whichGem == 1)
        {
            Instantiate(redObj, new Vector2(destroyGemX, 5), redObj.rotation);
        }
        if (whichGem == 2)
        {
            Instantiate(blueObj, new Vector2(destroyGemX, 5), blueObj.rotation);
        }
        if (whichGem == 3)
        {
            Instantiate(greenObj, new Vector2(destroyGemX, 5), greenObj.rotation);
        }
        if (whichGem > 3)
        {
            Instantiate(yellowObj, new Vector2(destroyGemX, 5), yellowObj.rotation);
        }


    }


    IEnumerator Wait(int time)
    {
        yield return new WaitForSeconds(time);
        lockDestroy = "n";
        
    }



}
